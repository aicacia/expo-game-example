import {
  Canvas as EngineCanvas,
  Scene,
  FullScreenCanvas,
  Time,
  Input,
  Entity,
  Camera2D,
  Transform2D,
  EventLoop,
} from "@aicacia/engine";
import { CtxRenderer, UITextCtxRendererHandler } from "@aicacia/engine/lib/web";
import { World2D } from "@aicacia/physics-2d";
import { vec2, vec4 } from "gl-matrix";
import { ExpoEventListener } from "./ExpoEventListener";
import { Canvas } from "./Canvas";
import { ExpoCanvas } from "./ExpoCanvas";
import { Client } from "./plugins/Client";
import { GameState } from "./plugins/GameState";
import {
  FixToCameraEdge,
  HorizontalPosition,
  VerticalPosition,
} from "./components/FixToCameraEdge";
import { DrawCtxRendererHandler } from "./plugins/renderers/DrawCtxRendererHandler";
import { Draw } from "./components/Draw";
import { DotCtxRendererHandler } from "./plugins/renderers/DotCtxRendererHandler";

export class Game {
  canvas: EngineCanvas;
  ctx: CanvasRenderingContext2D;
  eventLoop: EventLoop;
  scene: Scene;

  constructor(canvas: Canvas) {
    const input = new Input().addEventListener(new ExpoEventListener(canvas));

    this.canvas = new ExpoCanvas(canvas);
    this.ctx = canvas.getContext("2d") as any;
    this.scene = new Scene()
      .addPlugin(
        new Client(),
        input,
        new CtxRenderer(this.canvas, this.ctx).addRendererHandler(
          new DotCtxRendererHandler(),
          new UITextCtxRendererHandler(),
          new DrawCtxRendererHandler()
        ),
        new Time(),
        new GameState(),
        new FullScreenCanvas(this.canvas),
        new World2D()
      )
      .addEntity(
        new Entity().addComponent(
          new Transform2D(),
          new Camera2D().setBackground(vec4.fromValues(0.9, 0.9, 0.9, 1.0))
        ),
        new Entity().addTag("draw").addComponent(new Transform2D(), new Draw()),
        new Entity()
          .addComponent(
            new Transform2D(),
            new FixToCameraEdge()
              .setHorizontalPosition(HorizontalPosition.Left)
              .setVerticalPosition(VerticalPosition.Top)
          )
          .addChild(
            new Entity()
              .addTag("top-left")
              .addComponent(
                new Transform2D().setLocalPosition(vec2.fromValues(0.25, -0.25))
              )
          )
      );
    this.eventLoop = new EventLoop(input, () => this.scene.update());
  }

  startRoom() {
    console.log("Game Started");
    return this.scene.getRequiredPlugin(GameState).start();
  }
  joinRoom(room: string, name: string) {
    console.log(`Joined Room ${room} as ${name}`);
    return this.scene.getRequiredPlugin(Client).joinRoom(room, name);
  }

  start() {
    return this.scene.update();
  }
  stop() {
    return this;
  }
}
