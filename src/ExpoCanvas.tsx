import { Canvas } from "./Canvas";
import { Canvas as AicaciaCanvas } from "@aicacia/engine";

export class ExpoCanvas extends AicaciaCanvas {
  canvas: Canvas;

  constructor(canvas: Canvas) {
    super();

    this.canvas = canvas;
  }

  onResize() {
    this.canvas.set(this.getWidth(), this.getHeight());
    return this;
  }
}
