import React from "react";
import { Platform } from "react-native";
import { GCanvas } from "react-native-gcanvas";
import { CanvasComponent } from "./CanvasComponent";

export class Canvas extends CanvasComponent {
  private gcanvas: GCanvas | null = null;

  getContext(_contextId: string) {
    if (this.gcanvas) {
      return this.gcanvas.getContext();
    } else {
      return null;
    }
  }
  set(_width: number, _height: number) {
    throw new Error(`${Platform.OS} is not suppported yet`);
  }
  onGCanvas = (gcanvas: GCanvas) => {
    this.gcanvas = gcanvas;
  };

  onEvent = (type: string, event: any) => {
    event.type = event;
    this.eventEmitter.emit(type, event);
  };

  onResponderGrant = (event: any) => this.onEvent("touchstart", event);
  onResponderMove = (event: any) => this.onEvent("touchmove", event);
  onResponderReject = (event: any) => this.onEvent("touchcancel", event);
  onResponderTerminate = (event: any) => this.onEvent("touchcancel", event);
  onResponderRelease = (event: any) => this.onEvent("touchend", event);

  render() {
    return (
      <GCanvas
        ref={this.onGCanvas}
        onResponderGrant={this.onResponderGrant}
        onResponderMove={this.onResponderMove}
        onResponderReject={this.onResponderReject}
        onResponderTerminate={this.onResponderTerminate}
        onResponderRelease={this.onResponderRelease}
      />
    );
  }
}
