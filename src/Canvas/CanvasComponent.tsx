import React from "react";
import { EventEmitter } from "events";

export abstract class CanvasComponent extends React.PureComponent {
  protected eventEmitter: EventEmitter = new EventEmitter();

  getEventEmitter(): EventEmitter {
    return this.eventEmitter;
  }
  abstract getContext(contextId: string): CanvasRenderingContext2D | null;
  abstract set(width: number, height: number): void;
}
