import React from "react";
import { CanvasComponent } from "./CanvasComponent";

export class Canvas extends CanvasComponent {
  private element?: HTMLCanvasElement;

  getContext(contextId: string): CanvasRenderingContext2D | null {
    if (this.element) {
      return this.element.getContext(contextId) as CanvasRenderingContext2D;
    } else {
      return null;
    }
  }

  set(width: number, height: number) {
    if (this.element) {
      this.element.width = width;
      this.element.height = height;
      this.element.style.width = `${width}px`;
      this.element.style.height = `${height}px`;
    }
  }

  onCanvas = (element: HTMLCanvasElement) => {
    this.element = element;

    element.addEventListener("touchstart", this.onEvent);
    element.addEventListener("touchmove", this.onEvent);
    element.addEventListener("touchend", this.onEvent);
    element.addEventListener("touchcancel", this.onEvent);

    element.addEventListener("mousemove", this.onEvent);
    element.addEventListener("mousedown", this.onEvent);
    element.addEventListener("mouseup", this.onEvent);
    element.addEventListener("wheel", this.onEvent);
    element.addEventListener("mouseleave", this.onEvent);

    element.addEventListener("keydown", this.onEvent);
    element.addEventListener("keyup", this.onEvent);
  };

  onEvent = (event: Event) => {
    this.eventEmitter.emit(event.type, event);
  };

  render() {
    return <canvas ref={this.onCanvas} />;
  }
}
