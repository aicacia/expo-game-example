import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, View } from "react-native";
import { Provider, Modal, Portal, TextInput, Button } from "react-native-paper";
import { Canvas } from "./Canvas";
import { Game } from "./Game";
import { GameState } from "./plugins/GameState";
import { theme } from "./theme";

const RE_INVALID_CHARS = /[^a-z0-9]+/g;

export interface IAppState {
  started: boolean;
  inRoom: boolean;
  loading: boolean;
  room: string;
  name: string;
}

export class App extends React.Component<any, IAppState> {
  private game?: Game;

  state = {
    started: false,
    inRoom: false,
    loading: false,
    room: "test",
    name: "Nathan",
  };

  private onCanvas = (canvas: Canvas) => {
    const game = new Game(canvas);
    this.game = game;
    game.start();
  };

  componentWillUnmount() {
    if (this.game) {
      this.game.stop();
    }
  }

  onChangeRoom = (room: string) =>
    this.setState({ room: room.toLowerCase().replace(RE_INVALID_CHARS, "") });

  onChangeName = (name: string) => this.setState({ name });

  onCreateRoom = () =>
    this.setState({
      room: Math.random().toString(36).slice(2),
    });
  onJoinRoom = () => {
    this.game
      ?.joinRoom(this.state.room, this.state.name)
      .then(() => {
        this.setState({
          inRoom: true,
        });
        this.game?.scene.getRequiredPlugin(GameState).on("start", this.onStart);
      })
      .catch(() => {
        this.setState({
          loading: true,
        });
      });
    this.setState({
      loading: true,
    });
  };
  onStart = () => {
    if (this.game) {
      this.game.startRoom();
      this.setState({
        started: true,
      });
    }
  };

  render() {
    return (
      <Provider theme={theme}>
        <Canvas ref={this.onCanvas} />
        <Portal>
          <Modal
            visible={this.state.inRoom && !this.state.started}
            contentContainerStyle={AppStyles.form}
          >
            <Button mode="contained" onPress={this.onStart}>
              Start
            </Button>
          </Modal>
          <Modal
            visible={!this.state.inRoom}
            contentContainerStyle={AppStyles.form}
          >
            <TextInput
              label="Room ID"
              value={this.state.room}
              onChangeText={this.onChangeRoom}
            />
            <TextInput
              label="Name"
              value={this.state.name}
              onChangeText={this.onChangeName}
            />
            <View style={AppStyles.buttons}>
              <Button
                mode="contained"
                onPress={this.onCreateRoom}
                disabled={this.state.loading}
                style={AppStyles.button}
              >
                Create
              </Button>
              <Button
                mode="contained"
                onPress={this.onJoinRoom}
                disabled={
                  this.state.loading ||
                  (this.state.room.length < 4 && this.state.name.length > 0)
                }
                style={AppStyles.button}
              >
                Join
              </Button>
            </View>
          </Modal>
        </Portal>
        <StatusBar style="auto" />
      </Provider>
    );
  }
}

export const AppStyles = StyleSheet.create({
  canvas: {
    position: "absolute",
    width: "100%",
    height: "100%",
    top: 0,
    left: 0,
  },
  form: {
    margin: "auto",
    width: 256,
  },
  buttons: {
    display: "flex",
    flexWrap: "nowrap",
    flexDirection: "row",
  },
  button: {
    display: "flex",
    flex: 1,
  },
});
