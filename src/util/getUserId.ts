import AsyncStorage from "@react-native-community/async-storage";
import { Platform } from "react-native";
import { getDeviceId, getUserAgent } from "react-native-device-info";
import { v4, v5 } from "uuid";

const USER_UUID = "__WEB_USER_UUID__";

export async function getUserId() {
  if (Platform.OS === "web") {
    const uuidInAsyncStorage = await AsyncStorage.getItem(USER_UUID);

    if (uuidInAsyncStorage) {
      return uuidInAsyncStorage;
    } else {
      const uuid = v5(await getUserAgent(), v4());
      await AsyncStorage.setItem(USER_UUID, uuid);
      return uuid;
    }
  } else {
    return getDeviceId();
  }
}
