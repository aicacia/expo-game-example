import {
  EventListener,
  Pool,
  TouchInputEvent,
  MouseInputEvent,
  MouseWheelInputEvent,
  KeyboardInputEvent,
  ResizeInputEvent,
} from "@aicacia/engine";
import { Dimensions, ScaledSize } from "react-native";
import { Canvas } from "./Canvas";

export class ExpoEventListener extends EventListener {
  private touchInputEventPool = new Pool(TouchInputEvent);
  private mouseInputEventPool = new Pool(MouseInputEvent);
  private mouseWheelInputEventPool = new Pool(MouseWheelInputEvent);
  private keyboardInputEventPool = new Pool(KeyboardInputEvent);
  private resizeInputEventPool = new Pool(ResizeInputEvent);

  private canvas: Canvas;

  constructor(canvas: Canvas) {
    super();

    this.canvas = canvas;
  }

  onAdd() {
    const eventEmitter = this.canvas.getEventEmitter();

    Dimensions.addEventListener("change", this.onResize);

    eventEmitter.on("touchstart", this.onTouch);
    eventEmitter.on("touchmove", this.onTouch);
    eventEmitter.on("touchend", this.onTouch);
    eventEmitter.on("touchcancel", this.onTouch);

    eventEmitter.on("mousemove", this.onMouse);
    eventEmitter.on("mousedown", this.onMouse);
    eventEmitter.on("mouseup", this.onMouse);
    eventEmitter.on("wheel", this.onMouseWheel);
    eventEmitter.on("mouseleave", this.onMouse);

    eventEmitter.on("keydown", this.onKeyboard);
    eventEmitter.on("keyup", this.onKeyboard);

    this.onResize({ window: Dimensions.get("window") });

    return this;
  }

  onRemove() {
    const eventEmitter = this.canvas.getEventEmitter();

    Dimensions.removeEventListener("change", this.onResize);

    eventEmitter.off("touchstart", this.onTouch);
    eventEmitter.off("touchmove", this.onTouch);
    eventEmitter.off("touchend", this.onTouch);
    eventEmitter.off("touchcancel", this.onTouch);

    eventEmitter.off("mousemove", this.onMouse);
    eventEmitter.off("mousedown", this.onMouse);
    eventEmitter.off("mouseup", this.onMouse);
    eventEmitter.off("wheel", this.onMouseWheel);
    eventEmitter.off("mouseleave", this.onMouse);

    eventEmitter.off("keydown", this.onKeyboard);
    eventEmitter.off("keyup", this.onKeyboard);

    return this;
  }

  onResize = (e: { window: ScaledSize }) => {
    const event = this.resizeInputEventPool.create("resize");

    event.width = e.window.width;
    event.height = e.window.height;

    this.queueEvent(event);
  };

  onTouch = (e: Event) => {
    const touchEvent = e as TouchEvent;

    for (let i = 0, il = touchEvent.touches.length; i < il; i++) {
      const touch = touchEvent.touches[i],
        x = touch.clientX,
        y = touch.clientY,
        event = this.touchInputEventPool.create(touchEvent.type as any);

      event.id = touch.identifier;
      event.x = x;
      event.y = y;

      this.queueEvent(event);
    }
  };

  onMouse = (e: Event) => {
    const mouseEvent = e as MouseEvent,
      x = mouseEvent.clientX,
      y = mouseEvent.clientY,
      event = this.mouseInputEventPool.create(mouseEvent.type as any);

    event.button = mouseEvent.which;
    event.x = x;
    event.y = y;

    this.queueEvent(event);
  };

  onMouseWheel = (e: Event) => {
    const mouseWheelEvent = e as MouseWheelEvent,
      event = this.mouseWheelInputEventPool.create(mouseWheelEvent.type as any);

    event.wheel = mouseWheelEvent.deltaY;

    this.queueEvent(event);
  };

  onKeyboard = (e: Event) => {
    const keyEvent = e as KeyboardEvent,
      event = this.keyboardInputEventPool.create(keyEvent.type as any);

    event.code = keyEvent.code;

    this.queueEvent(event);
  };

  dequeueEvent(event: InputEvent) {
    if (event instanceof MouseInputEvent) {
      this.mouseInputEventPool.release(event);
      return true;
    } else if (event instanceof KeyboardInputEvent) {
      this.keyboardInputEventPool.release(event);
      return true;
    } else if (event instanceof MouseWheelInputEvent) {
      this.mouseWheelInputEventPool.release(event);
      return true;
    } else if (event instanceof TouchInputEvent) {
      this.touchInputEventPool.release(event);
      return true;
    } else {
      return false;
    }
  }
}
