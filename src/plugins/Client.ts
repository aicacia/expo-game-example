import { none, Option } from "@aicacia/core";
import { RunOnUpdatePlugin } from "@aicacia/engine";
import { Socket, Room, RoomEventType } from "@aicacia/p2p";
import { getUserId } from "../util/getUserId";

export const WS_URL = "wss://api.p2p.aicacia.com";

export interface IClientData {
  name: string;
}

export class Client extends RunOnUpdatePlugin {
  private socket: Option<Socket> = none();
  private room: Option<Room<any, IClientData>> = none();

  getId() {
    return this.socket.map((socket) => socket.getId());
  }
  getSocket() {
    return this.socket;
  }
  getRoom() {
    return this.room;
  }
  getPeers() {
    return this.room.map((room) => room.getPeers()).unwrapOr({});
  }

  joinRoom(roomName: string, name: string) {
    return this.socket
      .unwrap()
      .joinRoom<any, IClientData>(roomName, { name })
      .then(async (room) => {
        this.room.replace(room);
        this.emit("join", room);

        this.getRequiredPlugin(GameState).addPlayer(
          room.getId(),
          room.getData()
        );

        room.on(RoomEventType.PEER_JOIN, (peerId, data) =>
          this.runOnUpdate(() => {
            this.getRequiredPlugin(GameState).addPlayer(peerId, data);
          })
        );
        room.on(RoomEventType.PEER_LEAVE, (peerId) =>
          this.runOnUpdate(() => {
            console.log(peerId);
            this.getRequiredPlugin(GameState).removePlayer(peerId);
          })
        );
        room.on(RoomEventType.PEER_DATA_UPDATED, (peerId, data) =>
          this.runOnUpdate(() => {
            this.getRequiredPlugin(GameState).updatePlayer(peerId, data);
          })
        );

        return room;
      });
  }

  onInit() {
    getUserId().then((userId) => {
      const socket = new Socket(userId, {
        appId: "game-example",
        url: `${WS_URL}/socket`,
      });
      this.socket.replace(socket);
      return socket.connect();
    });
    return this;
  }

  onRemove() {
    this.socket.map((socket) => {
      this.room.take().map((room) => {
        this.emit("leave", room);
        room.leave();
      });
      socket.disconnect();
    });
    return this;
  }
}

import { GameState } from "./GameState";
