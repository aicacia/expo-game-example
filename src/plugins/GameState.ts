import { hash } from "@aicacia/hash";
import { shuffle, XorShiftRng } from "@aicacia/rand";
import { Entity, Input, Plugin, Transform2D, UIText } from "@aicacia/engine";
import { vec2, vec4 } from "gl-matrix";
import { Client, IClientData } from "./Client";
import { Dot } from "../components/Dot";
import { RoomEventType } from "@aicacia/p2p";
import { Draw } from "../components/Draw";

const VEC2_0 = vec2.create(),
  VEC4_0 = vec4.create();

export class GameState extends Plugin {
  private started = false;
  private playersTurnId = "";

  start() {
    if (!this.started) {
      this.started = true;

      const client = this.getRequiredPlugin(Client),
        peers = Object.keys(client.getPeers());

      peers.push(client.getId().unwrap());

      this.playersTurnId = shuffle(peers)[0];

      this.broadcast("start", { playersTurnId: this.playersTurnId });

      this.setDrawColor(
        GameState.getPeerColor(
          VEC4_0,
          this.getRequiredPlugin(Client).getId().unwrap()
        )
      );
    }
  }

  static getPeerColor(out: vec4, peerId: string) {
    const rng = XorShiftRng.fromSeed(Math.abs(hash(peerId)));

    return vec4.set(
      out,
      rng.nextFloatInRange(0.0, 0.75),
      rng.nextFloatInRange(0.0, 0.75),
      rng.nextFloatInRange(0.0, 0.75),
      1.0
    );
  }

  isMyTurn() {
    return this.getRequiredPlugin(Client)
      .getId()
      .map((id) => id === this.playersTurnId)
      .unwrapOr(false);
  }

  setDrawColor(color: vec4) {
    this.getRequiredScene()
      .findWithTag("draw")
      .map((entity) => entity.getRequiredComponent(Draw).setColor(color));
  }

  addPlayer(peerId: string, data: IClientData) {
    const parent = this.getRequiredScene().findWithTag("top-left").unwrap();

    parent.addChild(
      new Entity()
        .addTag(`player-${peerId}`)
        .addComponent(
          new Transform2D().setLocalPosition(
            vec2.set(VEC2_0, 0, parent.getChildren().length * -0.5)
          ),
          new Dot().setColor(GameState.getPeerColor(VEC4_0, peerId)),
          new UIText().setText(data.name)
        )
    );
  }
  removePlayer(peerId: string) {
    const parent = this.getRequiredScene().findWithTag("top-left").unwrap();

    parent
      .findAllWithTag(`player-${peerId}`)
      .map((entity) => parent.removeChild(entity));
  }
  updatePlayer(peerId: string, data: IClientData) {
    const parent = this.getRequiredScene().findWithTag("top-left").unwrap();

    parent
      .findAllWithTag(`player-${peerId}`)
      .map((entity) => entity.getRequiredComponent(UIText).setText(data.name));
  }

  onInit() {
    this.getRequiredPlugin(Client).on("join", (room) => {
      room.on(RoomEventType.MESSAGE, this.onMessage);
    });
    return this;
  }

  onRemove() {
    this.getRequiredPlugin(Client).on("leave", (room) => {
      room.off(RoomEventType.MESSAGE, this.onMessage);
    });
    return this;
  }

  private onMessage = (peerId: string, event: { type: string; data: any }) => {
    switch (event.type) {
      case "start": {
        this.playersTurnId = event.data.playersTurnId;
        this.started = true;
        this.emit("start");
        break;
      }
      default: {
        this.emit(event.type, peerId, event.data);
        this.getRequiredPlugin(Input).emit("event", event);
        break;
      }
    }
  };

  broadcast(type: string, data: any) {
    this.getRequiredPlugin(Client)
      .getRoom()
      .map((room) => room.broadcast({ type, data }));
  }
}
