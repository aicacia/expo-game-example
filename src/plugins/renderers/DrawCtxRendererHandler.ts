import { toRgba, TransformComponent } from "@aicacia/engine";
import { CtxRendererHandler } from "@aicacia/engine/lib/web";
import { mat2d } from "gl-matrix";
import { DrawManager } from "../../components/DrawManager";

const MAT2_0 = mat2d.create();

export class DrawCtxRendererHandler extends CtxRendererHandler {
  onRender() {
    this.getManager(DrawManager).ifSome((drawManager) => {
      const renderer = this.getRequiredRenderer(),
        scale = this.getScale();

      drawManager.getComponents().forEach((draw) =>
        draw
          .getEntity()
          .flatMap(TransformComponent.getTransform)
          .map((transform) =>
            renderer.render((ctx) => {
              draw.getLines().forEach((line) => {
                ctx.beginPath();
                ctx.lineWidth = scale * line.getSize();
                ctx.strokeStyle = toRgba(line.getColor());
                ctx.moveTo(line.getStart()[0], line.getStart()[1]);
                ctx.lineTo(line.getEnd()[0], line.getEnd()[1]);
                ctx.stroke();
                ctx.closePath();
              });
            }, transform.getMatrix2d(MAT2_0))
          )
      );
    });

    return this;
  }
}
