import { toRgb, TransformComponent } from "@aicacia/engine";
import { CtxRendererHandler } from "@aicacia/engine/lib/web";
import { mat2d } from "gl-matrix";
import { DotManager } from "../../components/DotManager";

const MAT2_0 = mat2d.create();

export class DotCtxRendererHandler extends CtxRendererHandler {
  onRender() {
    this.getManager(DotManager).ifSome((dotManager) => {
      const renderer = this.getRequiredRenderer();

      dotManager.getComponents().forEach((dot) =>
        dot
          .getEntity()
          .flatMap(TransformComponent.getTransform)
          .map((transform) =>
            renderer.render((ctx) => {
              ctx.beginPath();
              ctx.fillStyle = toRgb(dot.getColor());
              ctx.arc(0, 0, dot.getSize(), 0, 2 * Math.PI);
              ctx.fill();
            }, transform.getMatrix2d(MAT2_0))
          )
      );
    });

    return this;
  }
}
