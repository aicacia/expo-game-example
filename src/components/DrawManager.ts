import { DefaultManager } from "@aicacia/engine";

export class DrawManager extends DefaultManager<Draw> {}

import { Draw } from "./Draw";
