import { Component } from "@aicacia/engine";
import { vec4 } from "gl-matrix";
import { DotManager } from "./DotManager";

export class Dot extends Component {
  static Manager = DotManager;

  private color: vec4 = vec4.create();
  private size = 0.2;

  getColor() {
    return this.color;
  }
  setColor(color: vec4) {
    vec4.copy(this.color, color);
    return this;
  }

  getSize() {
    return this.size;
  }
  setSize(size: number) {
    this.size = size;
    return this;
  }
}
