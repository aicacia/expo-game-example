import { Camera2DManager, Component, Transform2D } from "@aicacia/engine";
import { vec2 } from "gl-matrix";

const VEC2_0 = vec2.create();

export enum HorizontalPosition {
  Center,
  Left,
  Right,
}
export enum VerticalPosition {
  Center,
  Top,
  Bottom,
}

export class FixToCameraEdge extends Component {
  static requiredComponents = [Transform2D];

  private horizontalPosition: HorizontalPosition = HorizontalPosition.Center;
  private verticalPosition: VerticalPosition = VerticalPosition.Center;

  getHorizontalPosition() {
    return this.horizontalPosition;
  }
  setHorizontalPosition(horizontalPosition: HorizontalPosition) {
    this.horizontalPosition = horizontalPosition;
    return this;
  }

  getVerticalPosition() {
    return this.verticalPosition;
  }
  setVerticalPosition(verticalPosition: VerticalPosition) {
    this.verticalPosition = verticalPosition;
    return this;
  }

  onUpdate() {
    const camera = this.getRequiredScene()
        .getRequiredManager(Camera2DManager)
        .getRequiredActive(),
      width = camera.getWidth(),
      height = camera.getHeight(),
      halfWidth = width * 0.5,
      halfHeight = height * 0.5;

    let x = 0,
      y = 0;

    switch (this.horizontalPosition) {
      case HorizontalPosition.Center:
        x = halfWidth;
        break;
      case HorizontalPosition.Right:
        x = width;
        break;
    }
    switch (this.verticalPosition) {
      case VerticalPosition.Center:
        y = halfHeight;
        break;
      case VerticalPosition.Bottom:
        y = height;
        break;
    }

    const position = vec2.set(VEC2_0, x, y);
    camera.toWorld(position, position);
    this.getRequiredComponent(Transform2D).setLocalPosition(position);

    return this;
  }
}
