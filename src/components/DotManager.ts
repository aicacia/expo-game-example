import { DefaultDescriptorManager } from "@aicacia/engine";

export class DotManager extends DefaultDescriptorManager<Dot> {}

import { Dot } from "./Dot";
