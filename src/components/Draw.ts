import {
  Camera2DManager,
  Input,
  RunOnUpdateComponent,
  Time,
} from "@aicacia/engine";
import { vec2, vec4 } from "gl-matrix";
import { GameState } from "../plugins/GameState";
import { DrawManager } from "./DrawManager";

const VEC2_0 = vec2.create(),
  VEC2_1 = vec2.create();

export class Line {
  private start: vec2 = vec2.create();
  private end: vec2 = vec2.create();
  private color: vec4 = vec4.create();
  private size = 3;

  getStart() {
    return this.start;
  }
  setStart(start: vec2) {
    vec2.copy(this.start, start);
    return this;
  }

  getEnd() {
    return this.end;
  }
  setEnd(end: vec2) {
    vec2.copy(this.end, end);
    return this;
  }

  getColor() {
    return this.color;
  }
  setColor(color: vec4) {
    vec4.copy(this.color, color);
    return this;
  }

  getSize() {
    return this.size;
  }
  setSize(size: number) {
    this.size = size;
    return this;
  }

  toJSON() {
    return {
      color: this.color,
      size: this.size,
      start: this.start,
      end: this.end,
    };
  }
}

export class Draw extends RunOnUpdateComponent {
  static Manager = DrawManager;

  private lines: Line[] = [];
  private color: vec4 = vec4.fromValues(0.1, 0.1, 0.1, 1);
  private size = 3;

  private lastMousePosition: vec2 = vec2.create();
  private newLines: Line[] = [];
  private lastTime = 0.0;

  getLines() {
    return this.lines;
  }
  addLine(line: Line) {
    this.lines.push(line);
    return this;
  }

  getColor() {
    return this.color;
  }
  setColor(color: vec4) {
    vec4.copy(this.color, color);
    return this;
  }

  getSize() {
    return this.size;
  }
  setSize(size: number) {
    this.size = size;
    return this;
  }

  onAdd() {
    this.getRequiredPlugin(GameState).on("drawLines", this.onDraw);
    return this;
  }

  onRemove() {
    this.getRequiredPlugin(GameState).off("drawLines", this.onDraw);
    return this;
  }

  onDraw = (_peerId: string, lines: Array<ReturnType<Line["toJSON"]>>) => {
    this.runOnUpdate(() =>
      lines.forEach((line) =>
        this.addLine(
          new Line()
            .setStart(line.start)
            .setEnd(line.end)
            .setColor(line.color)
            .setSize(line.size)
        )
      )
    );
    return this;
  };

  onUpdate() {
    super.onUpdate();

    const input = this.getRequiredPlugin(Input),
      time = this.getRequiredPlugin(Time),
      camera = this.getRequiredSceneManager(
        Camera2DManager
      ).getRequiredActive(),
      mousePosition = vec2.set(
        VEC2_0,
        input.getButtonValue("mouse-x"),
        input.getButtonValue("mouse-y")
      ),
      mouseWorldPosition = camera.toWorld(VEC2_1, mousePosition);

    if (input.isDown("mouse-1")) {
      const line = new Line()
        .setColor(this.color)
        .setSize(this.size)
        .setStart(this.lastMousePosition)
        .setEnd(mouseWorldPosition);
      this.newLines.push(line);
      this.addLine(line);
    }

    this.lastTime += time.getRealDelta();

    if (this.lastTime >= 1.0) {
      this.lastTime = 0.0;
      if (this.newLines.length > 0) {
        this.getRequiredPlugin(GameState).broadcast(
          "drawLines",
          this.newLines.slice()
        );
        this.newLines.length = 0;
      }
    }

    vec2.copy(this.lastMousePosition, mouseWorldPosition);

    return this;
  }
}
