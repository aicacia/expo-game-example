declare module "react-native-gcanvas" {
  import { Component } from "react";
  import { View } from "react-native";

  export class GCanvas extends Component<View.Props, any> {
    getContext(): CanvasRenderingContext2D;
  }
}
