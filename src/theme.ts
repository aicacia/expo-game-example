import { DefaultTheme } from "react-native-paper";

export const theme = {
  ...DefaultTheme,
  dark: false,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
  },
};
